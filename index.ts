import express, { type Express, type Request, type Response } from 'express';
import dotenv from 'dotenv';

// Configuración de archivo .env

dotenv.config();

// Create express app

const app: Express = express();
const port: string | number = process.env.PORT || 8000;

// Define the first route of the app
app.get('/', (req: Request, res: Response) => {
  // Send Hello World
  res.send(
    'Welcome to API Restful: Express + TS + Nodemon + Jest + Swagger + Mongoose'
  );
});

app.get('/hello', (req: Request, res: Response) => {
  // Send Hello World
  res.send('Welcome to  GET Route: hello!');
});

// Execute app and listen request to PORT

app.listen(port, () => {
  console.log(`EXPRESS SERVER: running at http://localhost:${port}`);
});
