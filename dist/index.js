"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
// Configuración de archivo .env
dotenv_1.default.config();
// Create express app
const app = (0, express_1.default)();
const port = process.env.PORT || 8000;
// Define the first route of the app
app.get("/", (req, res) => {
    // Send Hello World
    res.send("Welcome to API Restful: Express + TS + Nodemon + Jest + Swagger + Mongoose");
});
app.get("/hello", (req, res) => {
    // Send Hello World
    res.send("Welcome to  GET Route: hello!");
});
// Execute app and listen request to PORT
app.listen(port, () => {
    console.log(`EXPRESS SERVER: running at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map